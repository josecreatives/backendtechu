
const cors = require("cors");
const express = require("express");
require("dotenv").config();
const app = express();
const URL_BASE = "/techU/v1/";
const userFile = require("./user.json");
const body_parser = require("body-parser");

const request_json = require("request-json");
const URL_MLAB = "https://api.mlab.com/api/1/databases/techu13db/collections/";
const URL_MLAB_1 =
  "https://api.mlab.com/api/1/databases/apitechuelb5ed/collections/";
const apikey_mlab = "apiKey=" + process.env.API_KEY_MLAB;


const port = process.env.PORT || 3000;
const URL_WORK = "/apitechu/v1/";

var enableCORS = function (req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "*");
  next(); 
};
app.use(enableCORS);

app.listen(port, function () {
  console.log("Node JS escuchando en el puerto: " + port);
});

app.use(body_parser.json());

/**
 *typo: Funcion
 *Genera la query string para omitir el _id del documento mongodb.
 */
function getQuery_idIgnore() {
  return 'f={"_id":0}&';
}

/**
 *@method:getQueryParams
 *@description:Esta funcion genera la query string para filtras las busquedas de usuarios en el metodo GET.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getQueryParams(params) {
  let queryString = "q={";
  queryString += getQueryParamsEquals(params);
  queryString += getQueryParamsContains(params);
  queryString += getQueryParamsBetweent(params);
  queryString =
    queryString.length > 3
      ? queryString.substr(0, queryString.length - 1)
      : queryString;
  queryString += "}&";
  console.log("getQueryParams", queryString);
  return queryString;
}

/**
 *@method:getQueryParams
 *@description:Esta funcion genera la query string de acuerdo a los parametros que llegan en el en las busquedas de usuarios en el metodo GET.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getParamsPath(params) {
  let paramsPath = 'q={"id":"' + params.id + '"}&';
  console.log("getParamsPath", paramsPath);
  return paramsPath;
}

/**
 *@method:getParamsPathAccountIdUser
 *@description:Esta funcion genera la query string para filtrar por el id de account.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getParamsPathAccountIdUser(params) {
  let paramsPath = 'q={"id_user":"' + params.id + '"}&';
  console.log("getParamsPathAccount", paramsPath);
  return paramsPath;
}

/**
 *@method:getParamsPathAccounId
 *@description:Esta funcion genera la query de acuerdo a los parametros que llegan pro el path para el ccount.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getParamsPathAccounId(params) {
  let paramsPath = 'q={"id":"' + params.id + '"}&';
  console.log("getParamsPathAccount", paramsPath);
  return paramsPath;
}

/**
 *@method:getQueryParamsEquals
 *@description:Esta funcion genera la query de acuerdo a los parametros que llegan pro el path para el user con conector logico =.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getQueryParamsEquals(params) {
  let queryString = "";
  params.document_type != undefined && params.document_type != ""
    ? (queryString += '"document_type":"' + params.document_type + '",')
    : (queryString += "");
  params.country != undefined && params.country != ""
    ? (queryString += '"country":"' + params.country + '",')
    : (queryString += "");
  params.city != undefined && params.city != ""
    ? (queryString += '"city":"' + params.city + '",')
    : (queryString += "");
  console.log("getQueryParamsEquals", queryString);
  return queryString;
}

/**
 *@method:getQueryParamsContains
 *@description:Esta funcion genera la query de acuerdo a los parametros que llegan por el path para el user con conector logico %like%.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getQueryParamsContains(params) {
  let queryString = "";

  params.first_name != undefined && params.first_name != ""
    ? (queryString +=
        '"first_name":{"$regex": ".*' +
        params.first_name +
        '.*","$options":"i"},')
    : (queryString += "");
    params.last_name != undefined && params.last_name != ""
    ? (queryString +=
        '"last_name":{"$regex": ".*' +
        params.last_name +
        '.*","$options":"i"},')
    : (queryString += "");
  params.document_number != undefined && params.document_number != ""
    ? (queryString +=
        '"document_number":{"$regex": ".*' +
        params.document_number +
        '.*","$options":"i"},')
    : (queryString += "");
  params.email != undefined && params.email != ""
    ? (queryString +=
        '"email":{"$regex": ".*' + params.email + '.*","$options":"i"},')
    : (queryString += "");
  params.id_client != undefined && params.id_client != ""
    ? (queryString +=
        '"id_client":{"$regex": ".*' +
        params.id_client +
        '.*","$options":"i"},')
    : (queryString += "");

  console.log("getQueryParamsContains", queryString);
  return queryString;
}

/**
 *@method:getQueryParamsBetweent
 *@description:Esta funcion genera la query de acuerdo a los parametros que llegan por el path para el user con conector logico between.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getQueryParamsBetweent(params) {
  let queryString = "";
  if (
    params.creation_date_init != undefined &&
    params.creation_date_init &&
    (params.creation_date_end == undefined || params.creation_date_end == "")
  ) {
    params.creation_date_init != undefined && params.creation_date_init != "";
    queryString +=
      '"creation_date":{"$gte": "' + params.creation_date_init + '"},';
  } else if (
    params.creation_date_end != undefined &&
    params.creation_date_end != "" &&
    (params.creation_date_init == undefined || params.creation_date_init == "")
  ) {
    queryString +=
      '"creation_date":{"$lte": "' + params.creation_date_end + '"},';
  } else if (
    params.creation_date_end != undefined &&
    params.creation_date_end != "" &&
    params.creation_date_end != undefined &&
    params.creation_date_end != ""
  ) {
    queryString +=
      '"$and":[{"creation_date":{"$gte": "' +
      params.creation_date_init +
      '"} },{"creation_date":{"$lte": "' +
      params.creation_date_end +
      '"} }],';
  }
  console.log("getQueryBetweent", queryString);
  return queryString;
}

/**
 *@method:getBodyParamsEquals
 *@description:Esta funcion genera la query de acuerdo a los parametros que llegan por el body para el user con conector logico =.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getBodyParamsEquals(params) {
  console.log("getBodyParamsEquals", params);
  let queryString = "q={";
  params.document_type != undefined && params.document_type != ""
    ? (queryString += '"document_type":"' + params.document_type + '",')
    : (queryString += "");
  params.document_number != undefined && params.document_number != ""
    ? (queryString += '"document_number":"' + params.document_number + '",')
    : (queryString += "");
  params.password != undefined && params.dopasswordcument_number != ""
    ? (queryString += '"password":"' + params.password + '"')
    : (queryString += "");
  queryString += "}&";
  console.log("getBodyParamsEquals", queryString);
  return queryString;
}

/**
 *@method:getQueryParamsOrderByDes
 *@description:Esta funcion genera la query de acuerdo a un campo seleccionado para generar el Order By.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getQueryParamsOrderByDes(paramsName) {
  let querySort = 's={"' + paramsName + '": -1}&';
  console.log("getQueryParamsOrder", querySort);
  return querySort;
}

/**
 *@method:getQueryPaginator
 *@description:Esta funcion genera la query para utilizar paginado en la busqueda de usuarios.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getQueryPaginator(params) {
  let queryPaginator = "";
  queryPaginator +=
    params.rowsPerPage != undefined && params.rowsPerPage != 0
      ? "l=" + params.rowsPerPage + "&"
      : "";
  queryPaginator +=
    params.skippingRows != undefined && params.skippingRows != 0
      ? "sk=" + params.skippingRows + "&"
      : "";
  console.log("getQueryPaginator", queryPaginator);
  return queryPaginator;
}

/**
 *@method:Rest API - POST/singin
 *@description:Este metodo sirve para el inicio de sesion de un usuario a la aplicacion.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.post(URL_BASE + "singin", function (req, res) {
  console.log("POST:" + URL_BASE + "/singin");
  const http_client = request_json.createClient(URL_MLAB);
  let query =
    "user?" + getQuery_idIgnore() + getBodyParamsEquals(req.body) + apikey_mlab;
  console.log("http request: ", query);
  http_client.get(query, function (err, respuestaMLab, body) {
    console.log(
      "Error: ",
      err,
      "Respuesta MLab",
      JSON.stringify(),
      "Body",
      JSON.stringify(body)
    );
    let response = {};
    if (!err) {
      if (body.length == 1) {
        let id = body[0].id;
        let userApp = body[0];
        params = { id: id };
        let queryUpdateLogged = "user?" + getParamsPath(params) + apikey_mlab;
        let login = '{"$set":{"logged":true}}';
        console.log("queryUpdateLogged: ", queryUpdateLogged);
        http_client.put(queryUpdateLogged, JSON.parse(login), function (
          errPut,
          resPut,
          bodyPut
        ) {
          console.log("bodyPut: ", bodyPut);
          console.log("login: ", JSON.parse(login));

          if (errPut) {
            response = {
              status: 500,
              message: "Error al ejecutar el inicio de sesión",
            };
            res.status(500);
          } else {
            if (bodyPut.n == 1) {
              console.log("body n==1: ", userApp);
              response = userApp;
              res.status(200);
              
            } else {
              response = {
                status: 204,
                message: "Error al registrar la sesión",
              };
              res.status(204);
            }
          }
          res.send(response);
        });
        
      } else {
        response = { status: 500, message: "Usuario no encontrado" };
        res.status(500);
        res.send(response);
      }
    } else {
      response = { status: 500, message: "Error al iniciar sesión" };
      res.status(500);
      res.send(response);
    }
    console.log("response final: ", response);
   
  });
});

/**
 *@method:Rest API - GET/users
 *@description:Este metodo sirve para obtener lista de usuarios.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.get(URL_BASE + "users", function (req, res) {
  console.log("GET:" + URL_BASE + "/users");
  const http_client = request_json.createClient(URL_MLAB);
  let query =
    "user?" +
    getQuery_idIgnore() +
    getQueryParamsOrderByDes("creation_date") +
    getQueryPaginator(req.query) +
    getQueryParams(req.query) +
    apikey_mlab;
  console.log("http request: ", query);
  http_client.get(query, function (err, respuestaMLab, body) {
    console.log(
      "Error: ",
      err,
      "Respuesta MLab",
      JSON.stringify(respuestaMLab),
      "Body",
      JSON.stringify(body)
    );
    let response = {};
    if (err) {
      response = { status: 500, message: "Error al obtener resultados" };
      res.status(500);
    } else {
      if (body.length > 0) {
        response = body;
      } else {
        response = { status: 204, message: "No se encontraron resultados" };
        res.status(204);
      }
    }
    console.log("response: ", response);
    res.send(response);
  });
});

/**
 *@method:Rest API - GET/users/{id}
 *@description:Este metodo sirve para obtener un usuario por su Id.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.get(URL_BASE + "users/:id", function (req, res) {
  console.log("GET:" + URL_BASE + "/users/{id}");
  let query =
    "user?" + getQuery_idIgnore() + getParamsPath(req.params) + apikey_mlab;
  const http_client = request_json.createClient(URL_MLAB);
  console.log("http request: ", query);
  http_client.get(query, function (err, respuestaMLab, body) {
    console.log(
      "Error: ",
      err,
      "Respuesta MLab",
      JSON.stringify(respuestaMLab),
      "Body",
      JSON.stringify(body)
    );
    let response = {};
    if (err) {
      response = { status: 500, message: "Error al obtener resultados" };
      res.status(500);
    } else {
      if (body.length > 0) {
        response = body;
      } else {
        response = { status: 204, message: "No se encontraron resultados" };
        res.status(204);
      }
    }
    console.log("response: ", response);
    res.send(response);
  });
});

/**
 *@method:createGuid
 *@description:Este metodo sirve generar un codigo aleatorio GUID.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function createGuid() {
  function S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  }
  return (
    S4() +
    S4() +
    "-" +
    S4() +
    "-4" +
    S4().substr(0, 3) +
    "-" +
    S4() +
    "-" +
    S4() +
    S4() +
    S4()
  ).toLowerCase();
}

/**
 *@method:getBodyParamsToObject
 *@description:Este metodo sirve generar mapear el body de un usuario.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getBodyParamsToObject(body, id) {
  console.log("body: ", body);
  console.log("id: ", id);
  console.log("profile.name: ", body["profile"].name);
  let obj = {
    id: id,
    first_name: body.first_name,
    last_name: body.last_name,
    email: body.email,
    password: body.password,
    document_type: body.document_type,
    document_number: body.document_number,
    mobile_phone: body.mobile_phone,
    address: body.address,
    id_client: body.id_client,
    phone: body.phone,
    city: body.city,
    country: body.country,
    creation_date: body.creation_date,
    update_date: body.update_date,
    visible_amounts: body.visible_amounts,
    maximum_amount_operations: body.maximum_amount_operations,
    type: body.type,
    profile: {
      name: body.profile.name,
      code: body.profile.code,
    },
    employee: {
      code: body.employee.code,
      position: body.employee.position,
    },
  };
  console.log("Object: ", obj);
  return obj;
}

/**
 *@method:POST users/
 *@description:Este metodo sirve generar para crear un usuario.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.post(URL_BASE + "users/", function (req, res) {
  console.log("POST:" + URL_BASE + "/users/");
  let newId = createGuid();
  params = { id: newId };

  let queryGet =
    "user?" + getQuery_idIgnore() + getParamsPath(params) + apikey_mlab;
  let objToInsert = getBodyParamsToObject(req.body, newId);

  const http_client = request_json.createClient(URL_MLAB);

  console.log("http request: ", queryGet);
  http_client.get(queryGet, function (err, respuestaMLab, data) {
    if (data.length == 0) {
      let queryInsert = URL_MLAB + "user?" + apikey_mlab;
      http_client.post(queryInsert, objToInsert, function (
        error,
        respuestaMLab,
        body
      ) {
        console.log("respuestaMLab:" + JSON.stringify(respuestaMLab));
        console.log("body: " + JSON.stringify(body));

        let response = {};
        if (error) {
          response = { status: 500, message: "Error al insertar usuario" };
          res.status(500);
        } else {
          if (body.id != undefined && body.id != "") {
            response = body;
            res.status(201);
          } else {
            response = {
              status: 204,
              message: "Error al actualizar resultados",
            };
            res.status(204);
          }
        }
        console.log("response:" + JSON.stringify(response));

        res.send(body);
      });
    }
  });
});

/**
 *@method:PUT users/{id}
 *@description:Este metodo sirve generar para actualizar un usuario.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.put(URL_BASE + "users/:id", function (req, res) {
  console.log("PUT:" + URL_BASE + "users/{id}");
  let objToUpdate = getBodyParamsToObject(req.body, req.params.id);
  params = { id: req.params.id };
  const http_client = request_json.createClient(URL_MLAB);
  var update = '{"$set":' + JSON.stringify(objToUpdate) + "}";
  console.log("queryUpdate:", update);
  let queryUpdate = URL_MLAB + "user?" + getParamsPath(params) + apikey_mlab;
  http_client.put(queryUpdate, JSON.parse(update), function (
    error,
    respuestaMLab,
    body
  ) {
    console.log("respuestaMLab:" + JSON.stringify(respuestaMLab));
    console.log("body:" + JSON.stringify(body));
    let response = {};
    if (error) {
      response = { status: 500, message: "Error al actualizar resultados" };
      res.status(500);
    } else {
      if (body.n == 1) {
        response = body;
        res.status(200);
      } else {
        response = { status: 204, message: "Error al actualizar resultados" };
        res.status(204);
      }
    }
    console.log("response:" + JSON.stringify(response));
    console.log("status:" + res.status);
    res.send(response);
  });
});

/**
 *@method:DELETE users/{id}
 *@description:Este metodo sirve generar para eliminar un usuario.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.delete(URL_BASE + "users/:id", function (req, res) {
  console.log("DELETE:" + URL_BASE + "users/{id}");
  params = { id: req.params.id };
  let queryGet = "user?" + getParamsPath(params) + apikey_mlab;
  console.log("queryGet:", queryGet);
  httpClient = request_json.createClient(URL_MLAB);
  httpClient.get(queryGet, function (error, respuestaMLab, bodyGet) {
    var userGet = bodyGet[0];
    let queryDelete = "user/" + userGet._id.$oid + "?" + apikey_mlab;
    console.log("queryDelete:", queryDelete);
    httpClient.delete(queryDelete, function (err, resMLab, body) {
      let response = {};
      if (err) {
        response = { status: 500, message: "Error al insertar usuario" };
        res.status(500);
      } else {
        //if (body.id != undefined && body.id != "") {
        response = body;
        res.status(204);
        //} else {
        //response = { status: 204, message: "Error al actualizar resultados" };
        // res.status(204);
        // }
      }
      console.log("response:" + JSON.stringify(response));
      console.log("status:" + res.status);
      res.send(response);
    });
  });
});

/**
 *@method:POST singout
 *@description:Este metodo sirve cerrar sesion de un usuario.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.post(URL_BASE + "singout", function (req, res) {
  console.log("POST:" + URL_BASE + "/singout");
  const http_client = request_json.createClient(URL_MLAB);
  let query =
    "user?" + getQuery_idIgnore() + getBodyParamsEquals(req.body) + apikey_mlab;
  console.log("http request: ", query);
  http_client.get(query, function (err, respuestaMLab, body) {
    console.log(
      "Error: ",
      err,
      "Respuesta MLab",
      JSON.stringify(),
      "Body",
      JSON.stringify(body)
    );
    let response = {};
    if (!err) {
      if (body.length == 1) {
        let id = body[0].id;
        params = { id: id };
        let queryUpdateLogged = "user?" + getParamsPath(params) + apikey_mlab;
        let login = '{"$unset":{"logged":true}}';
        console.log("queryUpdateLogged: ", queryUpdateLogged);
        http_client.put(queryUpdateLogged, JSON.parse(login), function (
          errPut,
          resPut,
          bodyPut
        ) {
          console.log("bodyPut: ", bodyPut);
          console.log("login: ", JSON.parse(login));

          if (errPut) {
            response = {
              status: 500,
              message: "Error al ejecutar el inicio de sesión",
            };
            res.status(500);
          } else {
            if (bodyPut.n == 1) {
              response = bodyPut;
              res.status(200);
            } else {
              response = {
                status: 204,
                message: "Error al registrar la sesión",
              };
              res.status(204);
            }
          }
        });
      } else {
        response = { status: 500, message: "Usuario no encontrado" };
        res.status(500);
      }
    } else {
      response = { status: 500, message: "Error al iniciar sesión" };
      res.status(500);
    }
    console.log("response: ", response);
    res.send(response);
  });
});

/**
 *@method: GET users/:id/accounts
 *@description:Este metodo obtiene los accounts de un usuario.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.get(URL_BASE + "users/:id/accounts", function (req, res) {
  console.log("GET:" + URL_BASE + "/users/{id}/accounts");
  let query =
    "account?" +
    getQuery_idIgnore() +
    getQueryParamsOrderByDes("creation_date") +
    getParamsPathAccountIdUser(req.params) +
    apikey_mlab;
  const http_client = request_json.createClient(URL_MLAB);
  console.log("http request: ", query);
  http_client.get(query, function (err, respuestaMLab, body) {
    console.log(
      "Error: ",
      err,
      "Respuesta MLab",
      JSON.stringify(respuestaMLab),
      "Body",
      JSON.stringify(body)
    );
    let response = {};
    if (err) {
      response = { status: 500, message: "Error al obtener resultados" };
      res.status(500);
    } else {
      if (body.length > 0) {
        response = body;
      } else {
        response = { status: 204, message: "No se encontraron resultados" };
        res.status(204);
      }
    }
    console.log("response: ", response);
    res.send(response);
  });
});

/**
 *@method: getParamsToAccount
 *@description:Este metodo mapea el objeto account.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getParamsToAccount(body, id) {
  console.log("body: ", body);
  console.log("id: ", id);

  let obj = {
    id_user: body.id_user,
    account_name: body.account_name,
    account_alias: body.account_alias,
    account_number: body.account_number,
    account_number_cci: body.account_number_cci,
    id: id,
    available_balance: body.available_balance,
    countable_balance: body.countable_balance,
    id_currency: body.id_currency,
    type: body.type,
    creation_date: body.creation_date,
    update_date: body.update_date,
  };

  console.log("Object: ", obj);
  return obj;
}

/**
 *@method: POST users/:id/accounts
 *@description:Este metodo sirve para crear un Account asociado a un usuarios.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.post(URL_BASE + "users/:id/accounts", function (req, res) {
  console.log("POST:" + URL_BASE + "users/{id}/accounts");
  let newId = createGuid();
  params = { id: newId };
  req.body.id_user = req.params.id;
  let queryGet =
    "account?" +
    getQuery_idIgnore() +
    getParamsPathAccounId(params) +
    apikey_mlab;
  let objToInsert = getParamsToAccount(req.body, newId);

  const http_client = request_json.createClient(URL_MLAB);

  console.log("http request: ", queryGet);
  http_client.get(queryGet, function (err, respuestaMLab, data) {
    if (data.length == 0) {
      let queryInsert = URL_MLAB + "account?" + apikey_mlab;
      http_client.post(queryInsert, objToInsert, function (
        error,
        respuestaMLab,
        body
      ) {
        console.log("respuestaMLab:" + JSON.stringify(respuestaMLab));
        console.log("body: " + JSON.stringify(body));

        let response = {};
        if (error) {
          response = { status: 500, message: "Error al insertar account" };
          res.status(500);
        } else {
          if (body.id != undefined && body.id != "") {
            response = body;
            res.status(201);
          } else {
            response = {
              status: 204,
              message: "Error al actualizar resultados",
            };
            res.status(204);
          }
        }
        console.log("response:" + JSON.stringify(response));

        res.send(body);
      });
    }
  });
});

/**
 *@method: PUT users/{id}/accounts/{idAccount}
 *@description:Este metodo sirve para actualizar un account asociado a un usuarios.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.put(URL_BASE + "users/:id/accounts/:idAccount", function (req, res) {
  console.log("PUT:" + URL_BASE + "users/{id}/accounts/{idAccount}");
  req.body.id_user = req.params.id;
  let objToUpdate = getParamsToAccount(req.body, req.params.idAccount);
  params = { id: req.params.idAccount };
  const http_client = request_json.createClient(URL_MLAB);
  var update = '{"$set":' + JSON.stringify(objToUpdate) + "}";
  console.log("queryUpdate:", update);
  let queryUpdate =
    URL_MLAB + "account?" + getParamsPathAccounId(params) + apikey_mlab;
  http_client.put(queryUpdate, JSON.parse(update), function (
    error,
    respuestaMLab,
    body
  ) {
    console.log("respuestaMLab:" + JSON.stringify(respuestaMLab));
    console.log("body:" + JSON.stringify(body));
    let response = {};
    if (error) {
      response = { status: 500, message: "Error al actualizar resultados" };
      res.status(500);
    } else {
      if (body.n == 1) {
        response = body;
        res.status(200);
      } else {
        response = { status: 204, message: "Error al actualizar resultados" };
        res.status(204);
      }
    }
    console.log("response:" + JSON.stringify(response));
    console.log("status:" + res.status);
    res.send(response);
  });
});

/**
 *@method: getParamsToTransaction
 *@description:Este metodo sirve para mapear un objeto transaction.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
function getParamsToTransaction(body, id) {
  console.log("body: ", body);
  console.log("id: ", id);

  let obj = {
    id: id,
    number: body.number,
    date: body.date,
    description: body.description,
    reason: body.reason,
    amount: body.amount,
    id_account: body.id_account,
  };

  console.log("Object: ", obj);
  return obj;
}

/**
 *@method: POST accounts/{id}/transactions
 *@description:Este metodo sirve para actualizar una transaccion.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.post(URL_BASE + "accounts/:id/transactions", function (req, res) {
  console.log("POST:" + URL_BASE + "accounts/{id}/transactions");
  let newId = createGuid();
  let idAccount = req.params.id;
  params = { id: idAccount };
  req.body.id_account = idAccount;
  let queryGet =
    "account?" +
    getQuery_idIgnore() +
    getParamsPathAccounId(params) +
    apikey_mlab;

  let objToInsert = getParamsToTransaction(req.body, newId);

  const http_client = request_json.createClient(URL_MLAB);

  console.log("http request: ", queryGet);
  http_client.get(queryGet, function (err, respuestaMLab, data) {
    if (data.length == 1) {
      let queryUpdate =
        URL_MLAB + "account?" + getParamsPathAccounId(params) + apikey_mlab;
      //data[0].transactions.push(objToInsert);
      var update =
        '{"$push": {"transactions":' + JSON.stringify(objToInsert) + "}}";
      console.log("update: ", update);
      console.log("queryUpdate: ", queryUpdate);
      console.log("data: ", data);
      http_client.put(queryUpdate, JSON.parse(update), function (
        error,
        respuestaMLab,
        body
      ) {
        console.log("respuestaMLab:" + JSON.stringify(respuestaMLab));
        console.log("body: " + JSON.stringify(body));

        let response = {};
        if (error) {
          response = { status: 500, message: "Error al actualizar resultados" };
          res.status(500);
        } else {
          if (body.n == 1) {
            response = body;
            res.status(201);
          } else {
            response = {
              status: 204,
              message: "Error al actualizar resultados",
            };
            res.status(204);
          }
        }
        console.log("response:" + JSON.stringify(response));
        res.send(body);
      });
      //console.log("response:" + JSON.stringify(response));
    }
  });
});

/**
 *@method:Rest API - GET/parameters
 *@description:Este metodo sirve para obtener lista de parametrias.
 *@author: Jose E. Rodriguez Paredes.
 *@version: 17.09.2020
 */
app.get(URL_BASE + "parameters", function (req, res) {
  console.log("GET:" + URL_BASE + "/parameters");
  const http_client = request_json.createClient(URL_MLAB);
  let query =
    "parameter?" +
    getQuery_idIgnore() +
      apikey_mlab;
  console.log("http request: ", query);
  http_client.get(query, function (err, respuestaMLab, body) {
    console.log(
      "Error: ",
      err,
      "Respuesta MLab",
      JSON.stringify(respuestaMLab),
      "Body",
      JSON.stringify(body)
    );
    let response = {};
    if (err) {
      response = { status: 500, message: "Error al obtener resultados" };
      res.status(500);
    } else {
      if (body.length > 0) {
        response = body;
      } else {
        response = { status: 204, message: "No se encontraron resultados" };
        res.status(204);
      }
    }
    console.log("response: ", response);
    res.send(response);
  });
});