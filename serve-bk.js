//importar dependencias
const cors = require('cors');
const express = require('express');
require('dotenv').config();
const app = express();
const URL_BASE = '/techU/v1/';
const userFile = require('./user.json');
const body_parser = require('body-parser');

const request_json = require('request-json');
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu13db/collections/';
const URL_MLAB_1 = 'https://api.mlab.com/api/1/databases/apitechuelb5ed/collections/';
const apikey_mlab = 'apiKey='+ process.env.API_KEY_MLAB;




//configurar el puerto de escucha
const port = process.env.PORT || 3000;
const URL_WORK = '/apitechu/v1/';

var enableCORS = function(req, res, next) {
res.set("Access-Control-Allow-Origin", "*");
res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
res.set("Access-Control-Allow-Headers", "*");
next(); // middleware para decirle que pase por aquí primero
}
app.use(enableCORS);

app.listen(port, function(){
  console.log('Node JS escuchando en el puertorrsssssss3ssdd3fs222ss: ' + port);
});

app.use(body_parser.json());

//GET(Collection)
app.get(URL_BASE + 'users',
    function(request, response){
      console.log("Hola Mundo");
      //respuesta (sed) se envia siempre al finak
      //todo lo despues de esta send no funciona
      //response.send(userFile);
      response.status(200).send(userFile);
});
//GET con mlab
app.get(URL_BASE + 'usersDB',
    function(req, res){
      const http_client = request_json.createClient(URL_MLAB);
      console.log('cliente http: ' + URL_MLAB );
      var fn = req.query.firstName;
      var ln = req.query.lastName;
      var nr = req.query.numRows;
      var sr = req.query.skippingRows;
      console.log('fn: ' + fn );
      console.log('ln: ' + ln );
      console.log('nr: ' + nr );
      console.log('sr: ' + sr );
      if(fn==undefined){
        fn = '';
      }
      if(ln==undefined){
        ln = '';
      }
      if(nr==undefined){
        nr = '';
      }
      if(sr==undefined){
        sr = '';
      }
      var queryString1 ='';
      var queryString2='';
      var queryLimit = '';
      var querySk = '';
      if(fn!='' && ln!=''){
         queryString1 = 'q={"first_name": {"$regex": ".*'+fn+'.*","$options":"i"},"last_name": {"$regex": ".*'+ln+'.*","$options":"i"}}&';
      }else{
        if(fn!='' ){
           queryString1 = 'q={"first_name": {"$regex": ".*'+fn+'.*","$options":"i"}}&';

        }

        if(ln!=''){
           queryString2 = 'q={"last_name": {"$regex": ".*'+ln+'.*","$options":"i"}}&';

        }
      }
      if(nr!=''){
        queryLimit = 'l='+nr+'&';
        

     }
     if(sr!=''){
      querySk = 'sk='+sr+'&';
      

     }
      querySort = 's={"id_User": -1}&'

      let field  ='f={"_id":0}&';

      console.log('users?'+field+querySk+ querySort+queryLimit+queryString1+queryString2+apikey_mlab);
      http_client.get('users?'+field+ querySk+querySort+queryLimit+queryString1+queryString2+apikey_mlab,
        function(err, respuestaMLab, body){
          console.log('Error: ' + err);
          console.log('Respuesta MLab: ' + JSON.stringify());
          console.log('Body: ' + JSON.stringify(body));
          var response = {};
          if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
          } else {
            if(body.length > 0) {
            response = body;
            } else {
              response = {"msg" : "Usuario no encontrado."};
              res.status(404);
            }
          }
          res.send(response);
        });
    //  response.status(200).send({"msg":"cliente http creado correctamente"});
});
//GET to DB
app.get(URL_BASE + 'usersDB/:id',
    function(request, response){
      console.log(request.params.id);
      console.log('GET /api/2/usersDB/:id');
      let id = request.params.id;
      let queryString = 'q={"id_User":' + id + '}&';
      let field  ='f={"_id":0}&';
      const http_client = request_json.createClient(URL_MLAB);
      console.log('users?'+ field+queryString + apikey_mlab);
      http_client.get('users?'+ field+queryString + apikey_mlab,
        function(err, respuestaMLab, body){
          console.log(`mlab body: ${JSON.stringify(respuestaMLab)}`);
          var respuesta = {};
          if(err) {
            respuesta = {"msg" : "Error al recuperar users de mLab."}
            response.status(500);
          } else {
            if(body.length > 0) {
            respuesta = body;
            } else {
              respuesta = {"msg" : "Usuario no encontrado."};
              response.status(404);
            }
          }
          response.send(respuesta);
        });


});


app.get(URL_BASE + 'usersDB/:id/accounts',
    function(request, response){
    console.log('GET usersDB/:id/accounts');
    console.log('id: ' + request.params.id);
    let id = request.params.id;
    let queryString = 'q={"user_id":'+id+'}&';
    let fieldString  ='f={"_id":0}&';
    console.log('url: ' + 'account?'+queryString+fieldString+apikey_mlab);
    const http_client = request_json.createClient(URL_MLAB_1);
    http_client.get('account?'+queryString+fieldString+apikey_mlab,
    function(err, respuestaMLab, data){
      console.log('err: ' +JSON.stringify(err));
      console.log('respuestaMLab : ' + JSON.stringify(respuestaMLab));
      console.log('data: ' +JSON.stringify(data));
      let respuesta = {};
      if(err) {
        respuesta = {"msg" : "Error al recuperar cuentas del usuario"}
        response.status(500);
      } else {
        if(data.length > 0) {
        respuesta = data;
        } else {
          respuesta = {"msg" : "cuentas no encontradas."};
          response.status(404);
        }
      }
      response.send(respuesta);
    });

});


app.get(URL_BASE + 'usersDB/:id/accounts2',
    function(request, response){
    console.log('GET usersDB/:id/accounts2');
    console.log('id: ' + request.params.id);
    let id = request.params.id;
    let queryString = 'q={"id_User":'+id+'}&';
    let fieldString  ='f={"_id":0}&';
    console.log('url: ' + 'users?'+queryString+fieldString+apikey_mlab);
    const http_client = request_json.createClient(URL_MLAB);
    http_client.get('users?'+queryString+fieldString+apikey_mlab,
    function(err, respuestaMLab, data){
      console.log('err: ' +JSON.stringify(err));
      console.log('respuestaMLab : ' + JSON.stringify(respuestaMLab));
      console.log('data: ' +JSON.stringify(data));
      let respuesta = {};
      if(err) {
        respuesta = {"msg" : "Error al recuperar cuentas del usuario"}
        response.status(500);
      } else {
        if(data.length > 0) {
        respuesta = data[0].account;
        } else {
          respuesta = {"msg" : "cuentas no encontradas."};
          response.status(404);
        }
      }
      response.send(respuesta);
    });

});


//POST USER TO BD
app.post(URL_BASE + 'usersDB2/',
    function(request, response){
    console.log('POST usersDB2/');
    let queryString = 's={ "id_User" : -1 }&l=1&';
    let fieldString  ='f={"_id":0}&';
    var newUser = {
    "first_name" : request.body.first_name,
    "last_name" : request.body.last_name,
    "email" : request.body.email,
    "password" : request.body.password
    };
    const http_client = request_json.createClient(URL_MLAB);
    console.log('users?'+queryString+fieldString+apikey_mlab);
    http_client.get('users?'+queryString+fieldString+apikey_mlab,

    function(err, respuestaMLab, data){
      var respuesta = {};
        let newid =  data[0].id_User + 1;
        newUser["id_User"] = newid;
        console.log('Nuevo Id: ' + newid);
          http_client.post(URL_MLAB+"users?" + apikey_mlab, newUser ,
          function(error, respuestaMLab, body) {
              console.log('body: ' + JSON.stringify(body));
              response.status(200);
              response.send(body);
          });
    });
});


app.put(URL_BASE + 'usersBD/:id',
function(req, res) {
  let id = req.params.id;
  let queryString = 'q={"id_User":' + id + '}&';
  const http_client = request_json.createClient(URL_MLAB);
  http_client.get('users?'+ queryString + apikey_mlab,
      function(err, resMLab, body){
      var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
      console.log(cambio);
      http_client.put(URL_MLAB +'users?' + queryString + apikey_mlab, JSON.parse(cambio),
       function(error, respuestaMLab, body) {
         console.log("body:"+ body); // body.n == 1 si se pudo hacer el update
        //res.status(200).send(body);
        res.send(body);
       });
  });
  });

  app.post(URL_BASE + 'loginBD',
    function (req, res){
      console.log("POST/loginBD");
      let email = req.body.email;
      let pass = req.body.password;
      let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
      let limFilter = 'l=1&';
      let clienteMlab = request_json.createClient(URL_MLAB);
      console.log(queryString);
      clienteMlab.get('users?'+ queryString + limFilter + apikey_mlab,
        function(error, respuestaMLab, body) {
          if(!error) {
            if (body.length == 1) { // Existe un usuario que cumple 'queryString'
              let login = '{"$set":{"logged":true}}';
              clienteMlab.put('users?q={"id_User": ' + body[0].id_User + '}&' + apikey_mlab, JSON.parse(login),
              //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
                function(errPut, resPut, bodyPut) {
                  res.send({'msg':'Login correcto', 'user':body[0].email, 'id_User':body[0].id_User, 'first_name':body[0].first_name,'logged':body[0].logged});
                  // If bodyPut.n == 1, put de mLab correcto
                });
            }
            else {
              res.status(404).send({"msg":"Usuario no válido."});
            }
          } else {
            res.status(500).send({"msg": "Error en petición a mLab."});
          }
      });
  });

  app.delete(URL_BASE + "usersDB/:id",
    function(req, res){
      var id = Number.parseInt(req.params.id);
      var query = 'q={"id_User":' + id + '}&';
      // Interpolación de expresiones
      //var query2 = `q={"id":${id} }&`;
      //var query3 = "q=" + JSON.stringify({"id": id})
  // + '&';
      httpClient = request_json.createClient(URL_MLAB);
      httpClient.get('users?' +  query + apikey_mlab,
        function(error, respuestaMLab, bodyGet){
          var respuesta = bodyGet[0];
          httpClient.delete("users/" +  respuesta._id.$oid +'?' + apikey_mlab,
              function(err, resMLab, body) {
                var response = !err ? body : {
                  "msg" : "Error borrando usuario."
                }
              res.send(response);
            });
      });
    });


  app.post(URL_BASE + "logoutBD",
    function(req, res) {
      console.log("POST /logoutBD");
      var email = req.body.email;
      var queryString = 'q={"email":"' + email + '","logged":true}&';
      console.log(queryString);
      var  clienteMlab = request_json.createClient(URL_MLAB);
      clienteMlab.get('users?'+ queryString + apikey_mlab,
        function(error, respuestaMLab, body) {
          var respuesta = body[0]; // Asegurar único usuario
          if(!error) {
            if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
              let logout = '{"$unset":{"logged":true}}';
              clienteMlab.put('users?q={"id_User": ' + respuesta.id_User + '}&' + apikey_mlab, JSON.parse(logout),
              //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
                function(errPut, resPut, bodyPut) {
                  res.send({'msg':'Logout correcto', 'user':respuesta.email});
                  // If bodyPut.n == 1, put de mLab correcto
                });
              } else {
                  res.status(404).send({"msg":"Logout failed!"});
              }
          } else {
            res.status(500).send({"msg": "Error en petición a mLab."});
          }
      });
  });

//GET(instancia de un objeto)
app.get(URL_BASE + 'users/:id',
    function(request, response){
      console.log(request.params.id);
      let pos = request.params.id  - 1 ;
      let respuesta = userFile[pos] == undefined ? "200":"400"
      response.send(userFile[pos]);
});
//GET(iquery param)
app.get(URL_BASE + 'usersq',
    function(request, response){
      console.log(request.query.id);
      console.log(request.query.email);
      response.send({"msg":"encontrado"});
});

//POST al recuerso user
app.post(URL_BASE + 'users',
    function(request, response){
    console.log('POST en users');
    let tam = userFile.length ;
    let newUser = {
      "idUser":tam + 1,
      "firstName":request.body.firstName,
      "lastName":request.body.lastName,
      "email":request.body.email,
      "pasword" : request.body.pasword

    }
    console.log(newUser);
    userFile.push(newUser);
    response.send({"msg":"Usuario creado correctamente"});

});

//PUT
app.put(URL_BASE + 'users/:id',
    function(request, response){
    console.log('PUT en users');
    console.log(request.params.id);

    let idBuscar = request.params.id ;

    let newUser = {
      "idUser":idBuscar,
      "firstName":request.body.firstName,
      "lastName":request.body.lastName,
      "email":request.body.email,
      "pasword" : request.body.pasword

    }
    for(i=0; i<userFile.length; i++){
      if(userFile[i].id== idBuscar){
        userFile[i] = newUser;
        response.send({"msg":"Usuario actualizado"});
      }
    }

    console.log(newUser);
    response.send({"msg":"Usuario no encontrado"});

});

//PUT
app.delete(URL_BASE + 'users/:id',
    function(request, response){
    console.log('DELETE en users');
    console.log(request.params.id);
    let idBuscar = request.params.id ;
    for(i=0; i<userFile.length; i++){
      if(userFile[i].id== idBuscar){
        userFile.splice(i,1);
        response.send({"msg":"Usuario eliminado"});
      }
    }

    response.send({"msg":"Usuario no encontrado"});

});

//LOGIN
app.post(URL_WORK + 'login',
    function(request, response){
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.pasword;

    for(us of userFile){
      if(us.email==user){
        if(us.pasword==pass){
          us.logged = true;
          writeUserDataToFile(userFile);
          console.log('login correcto');
          response.send({"mensaje":"login correcto","idUsuario":+us.idUser});
        }else{
          console.log('login incorrecto');
          response.send({"msg":"login incorrecto"});

        }
      }
    }


});

//LOGIN
app.post(URL_WORK + 'logout/:id',
    function(request, response){
    let idBuscar = request.params.id ;
    for(i=0; i<userFile.length; i++){
      if(userFile[i].idUser== idBuscar){
        delete userFile[i].logged;
        writeUserDataToFile(userFile);
        console.log("usuario " + userFile[i].id + " logout correcto");
        response.send({"mensaje":"logout correcto","idUsuario":+userFile[i].idUser});
      }
    }
      response.send({"mensaje":"logout incorrecto"});
});

function writeUserDataToFile(data){
var fs =  require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json",jsonUserData, "utf8"),
   function(err){
     if(err){
       console.log(err);
     }else{
       console.log("Datos escrutos en user.json ");
     }
   }
}